import requests
from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator

API_KEY = ' ' - #ключ api
# BASE_URL- Current API
BASE_URL = 'http://api.openweathermap.org/data/2.5/weather?'
# Saint _petersburg - 498817
CITY_ID = '498817'

args = {
    'owner': 'Lost',
    'start_date': datetime(2023, 3, 4),
    'provide_context': True
}


def extract(**kwargs):
    ti = kwargs['ti']
    a = requests.get(BASE_URL, params={'id': CITY_ID, 'units': 'metric', 'lang': 'ru', 'APPID': API_KEY})
    b = a.json()
    print(b)
    ti.xcom_push(key='weather', value=b)


def transform(**kwargs):
    ti = kwargs['ti']
    b = ti.xcom_pull(key='weather', task_ids=['extract'])[0]
    curent_main = [
        b['weather'][0]['main'],
        b['main']['temp'],
        b['main']['feels_like'],
        b['main']['humidity'],
        b['wind']['speed'],
        str(datetime.fromtimestamp(b['dt'])),
        b['name'],
        str(datetime.fromtimestamp(b['sys']['sunrise'])),
        str(datetime.fromtimestamp(b['sys']['sunset']))]
    print(curent_main)
    ti.xcom_push(key='weather', value=curent_main)


with DAG('Am', description='weather', schedule_interval='1 * * * *', catchup=False, default_args=args) as dag:
    extract = PythonOperator(task_id='extract', python_callable=extract)
    transform = PythonOperator(task_id='transform', python_callable=transform)

    create = PostgresOperator(
        task_id="create",
        postgres_conn_id="grafana_t",
        sql="""/* создание основной таблицы*/
                    CREATE TABLE iF NOT EXISTS H_WEATHER(
                        MAIN CHAR(30) NOT NULL,
                        CUR_TEMP NUMERIC NOT NULL,
                        CUR_TEMP_FEEL NUMERIC NOT NULL,
                        HUMIDITY NUMERIC NOT NULL,
                        WIND NUMERIC NOT NULL,
                        DATE_TIME CHAR(50) NOT NULL,
                        COUNTRY CHAR(50) NOT NULL
                    ); """)
    insert_1 = PostgresOperator(
        task_id='ins1',
        postgres_conn_id="grafana_t",
        sql="""INSERT INTO H_WEATHER
                    (   MAIN, CUR_TEMP, CUR_TEMP_FEEL, HUMIDITY, WIND, DATE_TIME, COUNTRY)
                        values
                        ('{{ti.xcom_pull(key='weather', task_ids=['transform'])[0][0]}}',
                        {{ti.xcom_pull(key='weather', task_ids=['transform'])[0][1]}},
                        {{ti.xcom_pull(key='weather', task_ids=['transform'])[0][2]}},
                        {{ti.xcom_pull(key='weather', task_ids=['transform'])[0][3]}},
                        {{ti.xcom_pull(key='weather', task_ids=['transform'])[0][4]}},
                        '{{ti.xcom_pull(key='weather', task_ids=['transform'])[0][5]}}',
                        '{{ti.xcom_pull(key='weather', task_ids=['transform'])[0][6]}}');
                        """)

    create_sec = PostgresOperator(
        task_id="create_sec",
        postgres_conn_id="grafana_t",
        sql="""/* создание таблицы восход-закат*/

                CREATE TABLE iF NOT EXISTS SUN_R_S
                (	COUNTRY CHAR(50) NOT NULL ,
                    SUNRISE_TIME CHAR(50) NOT NULL,
                    SUNSET_TIME CHAR(50) NOT NULL
                );
                /* создание таблицы стейджинга восход-закат*/
                CREATE TABLE iF NOT EXISTS SUN_R_S_STG
                (	COUNTRY CHAR(50) NOT NULL,
                    SUNRISE_TIME CHAR(50) NOT NULL,
                    SUNSET_TIME CHAR(50) NOT NULL
            );

                            """)
    insert_2 = PostgresOperator(
        task_id='ins2',
        postgres_conn_id="grafana_t",
        sql="""   
                DELETE FROM SUN_R_S_STG;
                INSERT INTO SUN_R_S_STG
                     (COUNTRY, SUNRISE_TIME,SUNSET_TIME)
                        values 
                        ('{{ti.xcom_pull(key='weather', task_ids=['transform'])[0][6]}}',
                        '{{ti.xcom_pull(key='weather', task_ids=['transform'])[0][7]}}',
                        '{{ti.xcom_pull(key='weather', task_ids=['transform'])[0][8]}}');
                
                INSERT INTO SUN_R_S
                      (COUNTRY, SUNRISE_TIME,SUNSET_TIME)
                        SELECT
                        * FROM SUN_R_S_STG
                    WHERE NOT EXISTS
                    (SELECT 1 FROM SUN_R_S 
                     WHERE SUN_R_S.sunrise_time=SUN_R_S_STG.sunrise_time); """)

    extract >> transform >> create >> insert_1
    extract >> transform >> create_sec >> insert_2
